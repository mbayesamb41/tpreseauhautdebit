#!/bin/bash
#Mise en jour du système
echo "************************Mise à jour du système************************************"
sudo apt-get  update
#sudo apt-get upgrade
#les dépendances
echo "************************Installation des dépendances******************************"
rm var/lib/dpkg/lock-frontend
rm /var/lib/dpkg/lock
dpkg --configure -a
sudo apt-get install build-essential -y
sudo apt-get install cmake libfftw3-dev -y 
sudo apt-get install libmbedtls-dev -y
sudo apt-get install libboost-program-options-dev -y
sudo apt-get install libconfig++-dev -y 
sudo apt-get install libsctp-dev -y 
sudo apt-get install libtool -y
sudo apt-get install autoconf -y
sudo apt-get install libzmq3-dev -y
sudo apt-get install git -y
#Installation du module libzmq
echo "************************Installation de LZMQ************************************"
cd 
sudo git clone https://github.com/zeromq/libzmq.git
cd libzmq
./autogen.sh
./configure
sudo make
sudo make install
sudo ldconfig
#Installation de czmq
echo "************************Installation de CZMQ************************************"
cd 
sudo git clone https://github.com/zeromq/czmq.git
cd czmq
./autogen.sh
./configure
sudo make
sudo make install
sudo ldconfig
Installation de Srsran 
echo "************************Installation de SRSRAN************************************"
cd
sudo git clone https://github.com/srsRAN/srsRAN.git
cd srsRAN
sudo mkdir build 
cd build 
sudo cmake ../
sudo make 
sudo make test
sudo make install
#Génération des fichiers de configuration 
echo "************************Génération des fichiers de configuration*********************"
sudo srsran_install_configs.sh user
sudo srsran_install_configs.sh service
echo "*************************************************************************************"
echo "************************FIN DE L INSTALLATION DE SRSRAN******************************"
echo "*************************************************************************************"

#!/bin/bash
ip netns exec ue1  route add default gateway 172.16.0.1
sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sysctl -p
iptables -F
iptables -t nat -F
iptables -t nat -A POSTROUTING -s 172.16.0.0/24 ! -o srs_spgw_sgi -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.16.0.0/24 ! -o enp0s3 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.16.0.0/24 ! -o wlo1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.16.0.0/24 ! -o eth0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.16.0.0/24 ! -o wlp3s0 -j MASQUERADE
echo "*******************************************************************"
echo "                  ping ue1 vers google: 8.8.8.8"
echo "*******************************************************************"
ip netns exec ue1 ping 8.8.8.8

#!/bin/bash
#Suppression des les dépendances
echo "**********************Suppression des dépendances*********"
sudo apt-get purge --autoremove build-essential cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev libtool autoconf libzmq3-dev
#suppression des fichiers de configuration 
echo "**********************Suppression de lzmq*****************"
cd 
rm -r libzmq
echo "**********************Suppression de czmq*****************"
cd 
rm -r czmq
cd
rm -r srsRAN
rm -r .config/srsran
rm -r /etc/srsran


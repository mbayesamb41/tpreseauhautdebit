#!/bin/bash
cd 
ip netns del ue1
ip netns add ue1
gnome-terminal --maximize --tab -e 'bash -c "./srsRAN/build/srsepc/src/srsepc;$SHELL"' 
gnome-terminal --maximize --tab -e 'bash -c "./srsRAN/build/srsenb/src/srsenb  --rf.device_name=zmq --rf.device_args="fail_on_disconnect=true,tx_port=tcp://*:2000,rx_port=tcp://localhost:2001,id=enb,base_srate=23.04e6";$SHELL"'
gnome-terminal --maximize --tab -e 'bash -c "./srsRAN/build/srsue/src/srsue --rf.device_name=zmq --rf.device_args="fail_on_disconnect=true,tx_port=tcp://*:2001,rx_port=tcp://localhost:2000,id=ue,base_srate=23.04e6" --gw.netns=ue1;$SHELL"'


#!/bin/bash
#Desinstallation des dépendances
echo "**********************Desinstallation des dépendances***********************"
rm /var/lib/dpkg/lock
apt-get autoremove make g++ libsctp-dev lksctp-tools iproute2 -y
#Suppression du dossier UERANSIM
echo "**********************Desinstallation de UERANSIM***************************"
cd
rm -r UERANSIM
echo "*************************************************************************"
echo "*******************Desinstallation est terminée avec succès**************"
echo "*************************************************************************"

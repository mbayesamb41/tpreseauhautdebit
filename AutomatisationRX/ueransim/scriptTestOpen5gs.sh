#!/bin/bash
echo "***********************************************************"
echo "******************Test de Open5GS**************************"
echo "***********************************************************"
echo "              Ping vers google 8.8.8.8                     "
echo 1 > /proc/sys/net/ipv4/ip_forward
sysctl -p
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o uesimtun0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o wlo1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s25 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s3 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o eth0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o wlp3s0 -j MASQUERADE
ping -I uesimtun0 8.8.8.8



#!/bin/bash
echo "***************************************************************"
echo "CONFIGURATION DE UERANSIM"
echo "***************************************************************"
cd
echo "Donnez l'adresse IP de la machine UERANSIM :"
read ipUeransim
echo "Donnez l'adresse IP de la machine open5gs :"
read ipOpen5gs
cp -r UERANSIM/config UERANSIM/config2 
cp -r UERANSIM/config2/* UERANSIM/config/
sed -i s/999/901/g UERANSIM/config/open5gs-gnb.yaml
sed -i s/127.0.0.1/$ipUeransim/g UERANSIM/config/open5gs-gnb.yaml
sed -i s/127.0.0.5/$ipOpen5gs/g UERANSIM/config/open5gs-gnb.yaml
sed -i s/127.0.0.5/$ipUeransim/g UERANSIM/config/open5gs-ue.yaml
sed -i s/999/901/g UERANSIM/config/open5gs-ue.yaml
cd 
cd UERANSIM/build
gnome-terminal --maximize --tab -e 'bash -c "./nr-gnb -c ../config/open5gs-gnb.yaml;$SHELL"'
gnome-terminal --maximize --tab -e 'bash -c "./nr-ue -c ../config/open5gs-ue.yaml;$SHELL"'
echo 1 > /proc/sys/net/ipv4/ip_forward
sysctl -p
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o uesimtun0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o wlo1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s3 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o eth0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o wlp3s0 -j MASQUERADE




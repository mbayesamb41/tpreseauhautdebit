#!/bin/bash
#Mise à jour du système
echo "**********************Mise à jour du système*****************************"
apt-get update
#apt-get upgrade
#Installation des dépendances
echo "**********************Installation des dépendances***********************"
rm /var/lib/dpkg/lock
apt-get install make g++ libsctp-dev lksctp-tools iproute2 -y
snap refresh core
snap install cmake --classic
#Installation de UERANSIM
echo "**********************Installation de UERANSIM***************************"
cd
git clone https://github.com/aligungr/UERANSIM
cd UERANSIM
make
echo "*************************************************************************"
echo "**********************Installation est terminée avec succès**************"
echo "*************************************************************************"

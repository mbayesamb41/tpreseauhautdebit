#!/bin/bash
cd
cp -r /etc/open5gs /etc/open5gs2
cp /etc/open5gs2/* /etc/open5gs/
echo "donnez votre adresse IP :"
read ip
sed -i s/"tac: 7"/"tac: 1"/g /etc/open5gs/amf.yaml
sed -i s/127.0.0.5/$ip/g /etc/open5gs/amf.yaml
sed -i s/127.0.0.2/$ip/g /etc/open5gs/upf.yaml
service open5gs-amfd stop
service open5gs-upfd stop
gnome-terminal --maximize --tab -e 'bash -c "/usr/bin/open5gs-amfd -c /etc/open5gs/amf.yaml;$SHELL"'
gnome-terminal --maximize --tab -e 'bash -c "/usr/bin/open5gs-upfd -c /etc/open5gs/upf.yaml;$SHELL"'
echo 1 > /proc/sys/net/ipv4/ip_forward
sysctl -p
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o ogstun -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s3 -j MASQUERADE

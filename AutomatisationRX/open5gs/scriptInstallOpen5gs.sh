#!/bin/bash
#Mise en jour du système
echo "**********************Mise à jour du système***********************"
apt-get update -y
#Installtion des dépendances
echo **********************"Installatin des dépendances******************"
apt-get install make -y
apt-get install g++ -y
apt-get install libsctp-dev -y  
apt-get install lksctp-tools -y 
apt-get install iproute2 -y 
snap refresh core 
snap install cmake --classic
#Installation de Open5GS
echo "**********************Installation de Open5gs**********************"
apt-get update -y
apt-get install software-properties-common -y
add-apt-repository ppa:open5gs/latest -y
apt-get update -y
apt install open5gs -y 
#Installation de l'interface WEB de open5GS 
echo "**********************Installtion de l'interface WEB de Open5GS***"
apt update -y
apt install curl -y
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash - 
apt install nodejs -y
curl -fsSL https://open5gs.org/open5gs/assets/webui/install | sudo -E bash -
echo "*******************************************************************"
echo "*****************FIN DE L'INSTALLATION DE OPEN5GS******************"
echo "*******************************************************************"

#!/bin/bash
#Mise en jour du système
#Desinstalltion des dépendances
echo **********************"Desinstallatin des dépendances******************"
service open5gs-* stop
apt-get purge --autoremove  make -y
apt-get purge --autoremove g++ -y
apt-get purge --autoremove libsctp-dev -y
apt-get purge --autoremove lksctp-tools -y
apt-get purge --autoremove iproute2 -y
#Desinstallation de Open5GS
echo "**********************Desinstallation de Open5gs**********************"
#apt-get purge --autoremove software-properties-common -y
apt purge --autoremove open5gs -y 
#Desinstallation de l'interface WEB de open5GS 
echo "**********************Desinstalltion de l'interface WEB de Open5GS***"
apt purge --autoremove curl -y
apt purge --autoremove nodejs -y
#Supression des fichier de configuration 
echo "**************Supression des fichiers de configuration***************"
rm -r /usr/lib/node_modules/open5gs
rm -r /usr/lib/systemd/system/open5gs-*
rm -r /var/lib/systemd/deb-systemd-helper-enabled/open5gs-*
rm -r /var/lib/systemd/deb-systemd-helper-masked/open5gs-*
rm -r /var/lib/apt/lists/ppa.launchpad.net_open5gs-*
rm -r /var/lib/dpkg/info/open5gs-*
rm -r /etc/open5gs
rm -r /etc/apt/sources.list.d/open5gs-*
rm -r /usr/lib/node_modules/open5gs
rm - r /usr/lib/systemd/system/open5gs
rm -r /var/lib/systemd/deb-systemd-helper-masked/open5gs-*

echo "*******************************************************************"
echo "*****************FIN DE DESINSTALLATION DE OPEN5GS******************"
